const btnAccept = document.querySelector('.button-container button:last-of-type')
const btnReject = document.querySelector('.button-container button:first-of-type')

const element = document.querySelector('.container')

btnAccept.addEventListener("click", () => {
    // document.removeChild(element)
    toggleClass('right')
    setTimeout(()=>document.body.removeChild(element),400)
})

btnReject.addEventListener("click", () => {
    toggleClass('left')

})


function toggleClass(className) {
    // let element = document.querySelector('.container')
    element.classList.contains(className)? element.classList.remove(className): element.classList.add(className)
}