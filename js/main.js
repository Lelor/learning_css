const btnAccept = document.querySelector('.card button:last-child')
const btnReject = document.querySelector('.card button:first-child')


btnAccept.addEventListener("click", () => {
    toggleClass('right')
})

btnReject.addEventListener("click", () => {
    toggleClass('left')
})


function toggleClass(className) {
    let element = document.querySelector('.card')
    element.classList.contains(className)? element.classList.remove(className): element.classList.add(className)
}